import React from 'react';
import ReactDOM from 'react-dom/client';
import 'bootstrap/dist/css/bootstrap.min.css';
import App from './App';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  
    <App />
 
);


//JSX it is a syntax used in ReactJS
//Javascript + XML, it is an extension of JS that let us create objects which then be compiled and added as HTML elements

//With JSX, we are able to create HTML elements useing JS

//index.js is the main entry point. it is importing App, binding it to root in index.html